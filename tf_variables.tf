// General
variable "name" {
  description = "Name to be used on all the resources as identifier"
  type        = string
  default     = ""
}

variable "connection_type" {
  description = "The type for the dx connection "
  type        = string

  validation {
    condition     = can(regex("hosted|private", var.connection_type))
    error_message = "Supported values: hosted or private?"
  }
}

variable "interface_type" {
  description = "The type for the virtual interface"
  type        = string

  validation {
    condition     = can(regex("privat|public|transit", var.interface_type))
    error_message = "Please validate the Supported values: (public|private|transit)."
  }
}

variable "pvi_bgp_asn" {
  description = "The Autonomous System Number (ASN) of the customer side of the gateway"
  type        = number
}

variable "vpg_bgp_asn" {
  description = "The Autonomous System Number (ASN) of the customer side of the gateway"
  type        = number
}

variable "connection_id" {
  description = "The ID of the Direct Connect connection (or LAG) on which to create the virtual interface."
  type        = string

  validation {
    condition     = can(regex("^dxcon-", var.connection_id))
    error_message = "The provided value must be a valid DX Connection id, starting with \"dxcon-\"."
  }
}

variable "dx_gateway_id" {
  description = "The ID of the Direct Connect gateway to which to connect the virtual interface."
  type        = string
  default     = ""
}

variable "vlan" {
  description = "The virtual lan number (VLAN) of the customer side of the gateway"
  type        = number
}

variable "vpc_id" {
  description = "The VPC ID to create in"
  type        = string

  validation {
    condition     = can(regex("^vpc-", var.vpc_id))
    error_message = "The vpc_id value must be a valid VPC id, starting with \"vpc-\"."
  }
}

// Tags
variable "tags" {
  type        = map(string)
  description = "Tag that should be applied to all resources."
  default     = {}
}
