
resource "aws_vpn_gateway" "this" {
  vpc_id = var.vpc_id
  amazon_side_asn = var.vpg_bgp_asn

  tags = merge(var.tags, local.tags, {
    Name = lower("dx-vpg-${lower(var.name)}-tflz")
  })
}

resource "aws_vpn_gateway_route_propagation" "this" {
  vpn_gateway_id = aws_vpn_gateway.this.id
  route_table_id = "rtb-0fc6840070c3c4291"
}

resource "aws_dx_private_virtual_interface" "this" {
  connection_id  = var.connection_id
  vpn_gateway_id = aws_vpn_gateway.this.id

  name           = lower("dx-pvi-${lower(var.name)}-tflz")
  vlan           = var.vlan
  address_family = "ipv4"
  bgp_asn        = var.pvi_bgp_asn

  tags = merge(var.tags, local.tags, {
    Name = lower("dx-pvi-${lower(var.name)}-tflz")
  })
}